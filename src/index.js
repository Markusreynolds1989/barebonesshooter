import * as simple from "../simplegamelib/core.js";
import { Collision } from "../simplegamelib/collision.js";
import { Utility } from "../simplegamelib/utility.js";
let process, gameOver = false, player, enemies, shoot, bullets, bulletTimer, canShoot, score = 0, spawnRate = 100, canSpawn, life = 3, invCounter = 0;
const init = () => {
    player = { x: 200, y: 200, isActive: true, isInvincible: false, color: "white" };
    shoot = "down";
    bullets = [];
    enemies = [];
    bulletTimer = 50;
};
const draw = () => {
    simple.clear();
    if (player.isInvincible) {
        simple.drawRectangeLines(player.x, player.y, 25, 25, "blue");
    }
    if (!player.isInvincible) {
        simple.drawRectangeLines(player.x, player.y, 25, 25, "white");
    }
    for (let bullet of bullets) {
        simple.drawRectangle(bullet.x, bullet.y, 10, 10, "white");
    }
    for (let enemy of enemies) {
        simple.drawRectangle(enemy.x, enemy.y, 25, 25, "red");
    }
    simple.drawText(`Score: ${score}`, 650, 30, 18, "WHITE");
    simple.drawText(`Lives: ${life}`, 10, 30, 18, "white");
};
const handleKeys = () => {
    if (simple.upPressed && player.y > 0) {
        player.y -= 1;
        shoot = "up";
    }
    if (simple.downPressed && player.y < 800) {
        player.y += 1;
        shoot = "down";
    }
    if (simple.rightPressed && player.x < 800) {
        player.x += 1;
        shoot = "right";
    }
    if (simple.leftPressed && player.x > 0) {
        player.x -= 1;
        shoot = "left";
    }
    if (simple.spacePressed) {
        if (canShoot) {
            bulletTimer = 0;
            bullets.push({
                x: player.x + 10,
                y: player.y + 10,
                isActive: true,
                direction: shoot
            });
        }
    }
};
const update = () => {
    draw();
    if (gameOver) {
        clearInterval(process);
    }
    if (bulletTimer < 50) {
        bulletTimer += 1;
        canShoot = false;
    }
    if (bulletTimer == 50) {
        canShoot = true;
    }
    handleKeys();
    for (let bullet of bullets) {
        switch (bullet.direction) {
            case ("up"):
                bullet.y -= 1.25;
                break;
            case ("down"):
                bullet.y += 1.25;
                break;
            case ("left"):
                bullet.x -= 1.25;
                break;
            case ("right"):
                bullet.x += 1.25;
                break;
        }
        if (!Collision.inSceneBounds(bullet.x, bullet.y)) {
            bullet.isActive = false;
        }
        if (!bullet.isActive) {
            bullets.splice(bullets.indexOf(bullet), 1);
        }
    }
    if (spawnRate < 100) {
        spawnRate += 1;
        canSpawn = false;
    }
    if (spawnRate == 100 && enemies.length < 5) {
        canSpawn = true;
    }
    if (canSpawn) {
        spawnRate = 0;
        enemies.push({ x: Utility.random(0, 800), y: Utility.random(0, 800), isActive: true, isInvincible: false, color: "red" });
    }
    for (let enemy of enemies) {
        if (!enemy.isActive) {
            enemies.splice(enemies.indexOf(enemy), 1);
        }
        if (enemy.x < player.x) {
            enemy.x += 0.5;
        }
        if (enemy.x > player.x) {
            enemy.x -= 0.5;
        }
        if (enemy.y > player.y) {
            enemy.y -= 0.5;
        }
        if (enemy.y < player.y) {
            enemy.y += 0.5;
        }
        if (Collision.isRecTouching(enemy.x, enemy.y, 25, 25, player.x, player.y, 25, 25) && !player.isInvincible) {
            life -= 1;
            player.isInvincible = true;
            invCounter = 0;
        }
    }
    if (invCounter < 70 && player.isInvincible) {
        invCounter += 1;
    }
    if (invCounter == 70) {
        player.isInvincible = false;
    }
    for (let bullet of bullets) {
        for (let enemy of enemies) {
            if (Collision.isPointInRec(bullet.x, bullet.y, enemy.x, enemy.y, 25, 25)) {
                enemy.isActive = false;
                bullet.isActive = false;
                score += 10;
            }
        }
    }
    if (life < 0) {
        gameOver = true;
        simple.clear();
        simple.drawText("GameOver", 400, 400, 20, "white");
    }
};
const main = () => {
    init();
    process = setInterval(update, 10);
};
main();
